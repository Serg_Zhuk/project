Репозиторий интернет-магазина на Django 
Открыть терминал или консоль и перейти в нужную Вам директорию
Прописать команду `git clone git@gitlab.com:Serg_Zhuk/project.git
Прописать следующие команды:
- pip install virtualenv
- pip install virtualenvwrapper-win
- source ДиректорияВиртуальногоОкружения/bin/activate`
- Перейти в директорию **django3-ecommerce**
- pip install -r requirements.txt`
- python manage.py migrate`
- Запустить сервер - `python manage.py runserver`
- Не забудьте создать директорию **media**, куда будут сохраняться изображения для товара
