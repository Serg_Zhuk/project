
from django.forms import ModelChoiceField
from django.contrib import admin

from .models import *

from django import forms

class AutoSwitchCategoryChoiceField(forms.ModelChoiceField):
    pass


# делаем фильтрации для ввода одной категории
class AutoSwitchAdmin(admin.ModelAdmin):

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='autoswitchs'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class DiffbreakerAdmin(admin.ModelAdmin):

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='diffbreakers'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Category)
admin.site.register(AutoSwitch, AutoSwitchAdmin)
admin.site.register(Diffbreaker, DiffbreakerAdmin)
admin.site.register(CartProduct)
admin.site.register(Cart)
admin.site.register(Customer)
